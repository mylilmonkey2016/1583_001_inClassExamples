public class Bird extends Animal{

    public Bird(){
        super("Bird");
    }

    public void speak(){
        System.out.println("chirp chirp");
    }

    public void move(){
        System.out.println("The bird flies.");   
    }
}
