public class Human extends Animal{

    public Human(){
        super("Human");
    }

    public void speak(){
        System.out.println("blah blah blah...");
    }

    public void move(){
        System.out.println("The human trudges.");   
    }

    public void jump(){
        System.out.println("The human Jumps.");
    }
}
