public class Triangle extends Shape{
    private double base;
    private double height;

    public Triangle(double base, double height){
        super("triangle", 3);
        this.base = base;
        this.height = height;
    }

    public double getBase(){
        return this.base;
    }

    public double getHeight(){
        return this.height;
    }

    public double getArea(){
        return 0.5*getBase() * getHeight();
    }

    public String toString(){
        String str = super.toString();
        str += "Base: " + getBase() + "\n";
        str += "Height: " + getHeight() + "\n";
        str += "Area: " + getArea() + "\n";
        return str;
    }
}
