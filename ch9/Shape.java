public class Shape {
    private String name;
    private int numSides;

    public Shape(String name, int numSides){
        super();
        this.name = name;
        this.numSides = numSides;
    }

    public String getName(){
        return name;
    }

    public int getNumSides(){
        return numSides;
    }

    public String toString(){
        String str = "";
        str += getName() +"\n";
        str += getNumSides() + " num sides.\n";
        return str;
    }
}
