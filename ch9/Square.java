public class Square extends Shape{
    private int length;

    public Square(int length){
        super("square", 4);
        this.length = length;
        System.out.println("Finished building the Square");
    }

    public int getLength(){
        return this.length;
    }

    public int getArea(){
        return getLength() * getLength();
    }

    public String toString(){
        String str = super.toString();
        str += "Length: " + getLength() + "\n";
        str += "Area: " + getArea() + "\n";
        return str;
    }
}
