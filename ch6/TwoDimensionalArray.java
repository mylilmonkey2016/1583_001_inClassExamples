public class TwoDimensionalArray{
    public static void main(String[] args){
        int[][] arr1 = new int[10][10];
        int counter = 1;
        for(int i = 0; i < arr1.length; i++){
            for(int j = 0; j < arr1[i].length; j++){
                arr1[i][j] = counter;
                counter++;
            }
        }
        ArraysExample.printArray(arr1);
    }
}
