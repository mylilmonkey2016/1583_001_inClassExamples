public class ArraysExample{
    public static void main(String[] args){
        int[] evens = new int[5];

        printArray(evens);

        for(int i = 0; i < evens.length; i++){
            evens[i] = (i+1)*2;
        }

        System.out.println("after assigning values:\n");
        printArray(evens);
    }

    public static void printArray(int[] arr){
        for(int i = 0; i <= arr.length; i++){
            System.out.println(arr[i]);
        }
    }

    public static void printArray(int[][] arr){
        for(int row = 0; row < arr.length; row++){
            for(int col = 0; col < arr[row].length; col++){
                System.out.print(arr[row][col] + " ");
            }
            System.out.println();
        }
    }
}
