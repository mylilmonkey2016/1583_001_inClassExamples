import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.InterruptedException;
public class ConwaysGOL{
    private static int[][] currentBoard;
    private static int[][] nextBoard;
    public static void main(String[] args) throws FileNotFoundException{
        initializeBoards("gliderGun.txt");
        //printBoard(currentBoard);
        //System.out.println(getNumNeighbors(0,0)); // expect 0
        //System.out.println(getNumNeighbors(2,2)); // expect 5
        //System.out.println(getNumNeighbors(1,2)); // expect 1

        while(true){
            // print the board
            printBoard(currentBoard); // always prints currentBoard
            // generate the next board
            generateNextBoard(); // fills in values for nextBoard
            // swap the boards
            swapBoards();   
            sleep(100);
        }
    }

    public static void swapBoards(){
        for(int rowIndex = 0; rowIndex < currentBoard.length; rowIndex++){
            for(int colIndex = 0; colIndex < currentBoard[rowIndex].length; colIndex++){
                currentBoard[rowIndex][colIndex] = nextBoard[rowIndex][colIndex];
            }
        }
    }

    public static void generateNextBoard(){
        for(int rowIndex = 0; rowIndex < currentBoard.length; rowIndex++){
            for(int colIndex = 0; colIndex < currentBoard[rowIndex].length; colIndex++){
                int numLiveNeighbors = getNumNeighbors(rowIndex, colIndex);
                nextBoard[rowIndex][colIndex] = getNextStateOfCell(numLiveNeighbors, 
                                                    currentBoard[rowIndex][colIndex]);
            }                
        }
    }

    public static int getNextStateOfCell(int numLiveNeighbors, int currentState){
        int nextState = -1;
        if(numLiveNeighbors < 2 || numLiveNeighbors > 3){
            nextState = 0;
        } else if(numLiveNeighbors == 3){
            nextState = 1;
        } else if(numLiveNeighbors == 2){
            nextState = currentState;
        }
        
        return nextState;   
    }

    // this method reads from the currentBoard
    public static int getNumNeighbors(int rowIndex, int colIndex){
        int numNeighbors = 0;
        for(int i = rowIndex-1; i <= rowIndex+1; i++){
            for(int j = colIndex-1; j <= colIndex+1; j++){
                int numRows = currentBoard.length;       
                int numCols = currentBoard[0].length;       
                if(i >= 0 && i <= numRows-1 && 
                    j >= 0 && j <= numCols-1){
                    numNeighbors += currentBoard[i][j];
                }
            }
        }
        numNeighbors = numNeighbors - currentBoard[rowIndex][colIndex];
        return numNeighbors;
    }

    public static void initializeBoards(String inputFileName) throws FileNotFoundException{
        File file = new File(inputFileName);
        Scanner fileReader = new Scanner(file);
        int numRows = fileReader.nextInt();   
        int numCols = fileReader.nextInt();   
        System.out.println("Reading in a board with "+numRows +" rows" + " and " + numCols + " cols.");

        currentBoard = new int[numRows][numCols];
        nextBoard = new int[numRows][numCols];

        for(int rowIndex = 0; rowIndex < numRows; rowIndex++){
            for(int colIndex = 0; colIndex < numCols; colIndex++){
                currentBoard[rowIndex][colIndex] = fileReader.nextInt();
            }
        }
    }

    public static void printBoard(int[][] board){
        for(int rowIndex = 0; rowIndex < board.length; rowIndex++){
            for(int colIndex = 0; colIndex < board[rowIndex].length; colIndex++){
                int cellValue = currentBoard[rowIndex][colIndex];
                if(cellValue == 0){
                    System.out.print("- ");
                } else {
                    System.out.print("X ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void sleep(long milliseconds){
        try{
            Thread.sleep(milliseconds);
        } catch(InterruptedException e){
            e.printStackTrace();
        }
    }

}
