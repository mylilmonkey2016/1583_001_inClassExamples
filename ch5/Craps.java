import java.util.Random;
public class Craps{
    private static Random gen = new Random(); // let it use system time as seed
    private static enum Status {WON, LOST, CONTINUE};

    public static void main(String[] args){
        int thePoint = -1;       
        Status gameStatus;
        
        System.out.println("initial roll");
        int diceRoll = rollDice();
        if(diceRoll == 7 || diceRoll == 11){
            gameStatus = Status.WON;
        } else if(diceRoll == 2 || diceRoll == 3 || diceRoll == 12){
            gameStatus = Status.LOST;
        } else {
            thePoint = diceRoll;
            gameStatus = Status.CONTINUE;
            System.out.println("The point is: " + thePoint);
        }

        while(gameStatus == Status.CONTINUE){
            diceRoll = rollDice();

            if(diceRoll == thePoint){
                gameStatus = Status.WON;
            } else if (diceRoll == 7){
                gameStatus = Status.LOST;
            }
        }

        if(gameStatus == Status.WON){
            System.out.println("You won!");
        } else if (gameStatus == Status.LOST){
            System.out.println("You lost!");
        }
    }

    public static int rollDice(){
        int sum = 0;
        int dieValue1 = gen.nextInt(6) + 1;
        sum += dieValue1;
        int dieValue2 = gen.nextInt(6) + 1;
        sum += dieValue2;
        System.out.println("Rolled (" + dieValue1 + ", " + dieValue2 + ") sum: " +sum);
        return sum;
    }
}
