import java.util.Scanner;

public class HelloWorld{
	public static void main(String[] args){
		System.out.println("Enter your name: ");
		Scanner inputReader = new Scanner(System.in);
		String name;
		name = inputReader.next();

		System.out.println("Hello " + name);
	}
}