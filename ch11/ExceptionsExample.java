import java.util.Scanner;
import java.util.InputMismatchException;

public class ExceptionsExample{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        boolean cont = true;
        while(cont){
            try {
                System.out.print("enter first num: ");
                int num1 = reader.nextInt();

                System.out.print("enter second num: ");
                int num2 = reader.nextInt();

                int ans = divide(num1, num2);
                System.out.printf("%d / %d = %d\n", num1, num2, ans);
                cont = false;
            } catch ( ArithmeticException e ){
                System.out.println("Cannot divide by ZERO. try again.");
            } catch ( InputMismatchException e){
                System.out.println("You must enter ints. try again!");
                reader.nextLine();
            }
        }

    }

    public static int divide(int a, int b) throws ArithmeticException {
        return a/b;
    }
}
