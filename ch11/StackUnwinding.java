public class StackUnwinding {
    public static void main(String[] args){
        try {
            System.out.println("main(): calling a()");
            a();
        } catch (ArithmeticException e){
            System.out.println("main(): catch ArithmeticException");
            e.printStackTrace();
        } finally {
            System.out.println("main(): Finally Block");
        }
    }

    public static void a(){
        try{ 
            System.out.println("a(): calling b()");
            b();
        } catch(NullPointerException e){
            System.out.println("a(): catch NullPointerException");
            e.printStackTrace();
            System.out.println("a() Catch Block: throwing ArithmeticException");
            ArithmeticException ae = new ArithmeticException();   
            ae.initCause(e);
            throw ae;
        } finally {
            System.out.println("a(): Finally Block");
        }
    }

    public static void b(){
        //System.out.println("b(): throwing ArithmeticException");
        //throw new ArithmeticException();   
        System.out.println("b(): throwing NullPointerException");
        throw new NullPointerException();   
    }

}
