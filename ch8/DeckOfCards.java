import java.util.Random;
public class DeckOfCards{
    private Card[] deck;
    private int currentCard;

    public DeckOfCards(){

        deck = new Card[52];

        String[] faces = {"Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"};
        String[] suits = {"Spades", "Clubs", "Diamonds", "Hearts"};

        int i = 0;
        for(int suit = 0; suit < 4; suit++){
            for(int face = 0; face < 13; face++){
                deck[i] = new Card(faces[face], suits[suit]);
                i++;
            }
        }

        currentCard = 0;
    }

    public void shuffle(){
        Random gen = new Random();
        for(int i = 0; i < 52; i++){
            int newLocation = gen.nextInt(52);
            Card tempCard = deck[i];
            deck[i] = deck[newLocation];
            deck[newLocation] = tempCard;
        }
        currentCard = 0;
    }

    public Card dealCard(){
        Card cardToReturn;
        if(currentCard < 52){
            cardToReturn = deck[currentCard];
            currentCard++;
        } else {
            cardToReturn = null;
        }
        return cardToReturn;
    }
}
